import urllib.request
from pathlib import Path
from func.config import CONFIG

class Downloader:
    def downloadFile(self, url, filename="thumbnail"):
        path = CONFIG["STORAGE_PATH"] + "/" + filename + Path(url).suffix
        urllib.request.urlretrieve(url, path)
        return path

DownloadHandler = Downloader()
