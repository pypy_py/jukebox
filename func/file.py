import os
import json

def write(path, data):
    with open(path, "w+") as f:
        json.dump(data, f)

def read(path):
    with open(path, "r") as f:
        try:
            return json.loads(f.read())
        except ValueError:
            return None

def has(path):
    return os.path.isfile(path)

def remove(path):
    os.remove(path)
