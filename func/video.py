import youtube_dl
import ffmpeg
import logging
from pathlib import Path
from func.config import CONFIG
from func.downloader import DownloadHandler
from func import file

def trim(input_path, output_path, start=0, end=140):
    input_stream = ffmpeg.input(input_path)

    vid = (
        input_stream.video
        .trim(start=start, end=end)
        .setpts('PTS-STARTPTS')
    )
    aud = (
        input_stream.audio
        .filter_('atrim', start=start, end=end)
        .filter_('asetpts', 'PTS-STARTPTS')
    )

    joined = ffmpeg.concat(vid, aud, v=1, a=1).node
    output = ffmpeg.output(joined[0], joined[1], output_path).global_args("-y")
    output.run()

def get(data):
    video_path = CONFIG["STORAGE_PATH"] + "/" + data["_id"] + ".mp3"
    ydl = youtube_dl.YoutubeDL({
        'outtmpl': video_path,
        "format": "bestaudio/best",
        "postprocessors": [{
            "key": "FFmpegExtractAudio",
            "preferredcodec": "mp3",
        }]
    })

    if not file.has(video_path):
        with ydl:
            logging.info("Downloading {name}".format(name=data["name"]))
            ydl.download(["https://www.youtube.com/watch?v={id}".format(id=data["path"])])

    thumbnail_path = CONFIG["STORAGE_PATH"] + "/" + data["_id"] + Path(data["img_src"]).suffix
    if not file.has(thumbnail_path):
        logging.info("Downloading thumbnail")
        DownloadHandler.downloadFile(data["img_src"], data["_id"])

    logging.info("Converting into usable video")

    img = ffmpeg.input(thumbnail_path, r=1,loop=1)
    input = ffmpeg.input(video_path)

    output_path = CONFIG["STORAGE_PATH"] + "/" + "output.mp4"
    (
        ffmpeg
        .output(img, input, output_path,shortest=None, vcodec="libx264", r=1, codec="copy", strict="-2")
        .global_args("-y")
        .run()
    )

    trimmed_path = CONFIG["STORAGE_PATH"] + "/FINAL_VIDEO.mp4"
    trim(output_path, trimmed_path)

    file.remove(video_path)
    file.remove(output_path)
    file.remove(thumbnail_path)

    logging.info("Video processed")

    return trimmed_path
